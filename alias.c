#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "alias.h"

// array to make sure the variables sent back to main get freed
char *string_to_clear_al[MAX_STRING_CLEAR];
int count_clear_string_al;

// initialise la structure des alias
struct AliasList *init_aliases() {

  struct AliasList *aliases = malloc(sizeof(struct AliasList));
  if (aliases == NULL) {
    // initialization failed
    perror("malloc");
    exit(2);
  }

  // met les chaines de caractère à zéro
  for (int i = 0; i < ALIAS_MAX; i++) {
    aliases->input[i][0] = '\0';
    aliases->output[i][0] = '\0';
    aliases->nb = 0;
  }

  return aliases;
}

// ajoute un alias (retourne 1 en cas d'erreur)
int add_alias(struct AliasList *alias, char *in, char *out) {
  // si on est plein
  if (alias->nb == ALIAS_MAX) {
    // retourner erreur (1)
    return 1;
  }

  // pas de chaine nulle acceptée
  if (strlen(in) == 0 || strlen(out) == 0) {
    return 2;
  }

  // comtper les longeurs et rejetter les chaines trop longues (>=
  // ALIAS_MAX_SIZE)
  size_t inlen, outlen;
  inlen = strlen(in);
  outlen = strlen(out);
  if (inlen >= ALIAS_MAX_SIZE || outlen >= ALIAS_MAX_SIZE) {
    return 3;
  }

  // incrémenter nb et strcopier les nouvelles valeurs
  strcpy(alias->input[alias->nb], in);
  strcpy(alias->output[alias->nb], out);
  alias->nb = alias->nb + 1;

  return 0;
}

// chercher et remplace un alias (retourn 1 en cas d'erreur)
char *replace_alias(struct AliasList *alias, char *text) {
  // buffer pour comparer
  char bufcmp[ALIAS_MAX_SIZE + 2];
  // longeur de la chaine dont on remplace
  size_t len_text = strlen(text);
  // pour chaque alias
  for (int i = 0; i < alias->nb; i++) {
    // chercher occurence du mot suivi d'un saut de ligne seulement au début
    // (pour simplifier)
    bufcmp[0] = '\0';
    strcat(bufcmp, alias->input[i]);
    strcat(bufcmp, "\n");

    char *location = strstr(text, bufcmp);

    // si la chaine est au début
    if (location == text) {
      // chaine a inserer
      char *a_inserer;
      a_inserer = malloc(len_text + ALIAS_MAX_SIZE);
      a_inserer[0] = '\0';
      strcat(a_inserer, alias->output[i]);
      strcat(a_inserer, location + strlen(alias->input[i]));
      // on le marque comme nécessitant un free ou on vide le tableau d'abord
      if (count_clear_string_al > MAX_STRING_CLEAR - 10) {
        free_alias_orphan_strings();
      }
      string_to_clear_al[count_clear_string_al] = a_inserer;
      count_clear_string_al++;
      // retourner
      return a_inserer;
    }

    // chercher occurence du mot seulement au début suivi d'un espace (pour
    // simplifier)
    bufcmp[0] = '\0';
    strcat(bufcmp, alias->input[i]);
    strcat(bufcmp, " ");

    location = strstr(text, bufcmp);

    // si la chaine est au début
    if (location == text) {
      // chaine a inserer
      char *a_inserer;
      a_inserer = malloc(len_text + ALIAS_MAX_SIZE);
      a_inserer[0] = '\0';
      strcat(a_inserer, alias->output[i]);
      strcat(a_inserer, location + strlen(alias->input[i]));
      // on le marque comme nécessitant un free ou on vide le tableau d'abord
      if (count_clear_string_al > MAX_STRING_CLEAR - 10) {
        free_alias_orphan_strings();
      }
      string_to_clear_al[count_clear_string_al] = a_inserer;
      count_clear_string_al++;
      // retourner
      return a_inserer;
    }

    // chercher occurence du mot seulement au début suivi d'un \t (pour
    // simplifier)
    bufcmp[0] = '\0';
    strcat(bufcmp, alias->input[i]);
    strcat(bufcmp, "\t");

    location = strstr(text, bufcmp);

    // si la chaine est au début
    if (location == text) {
      // chaine a inserer
      char *a_inserer;
      a_inserer = malloc(len_text + ALIAS_MAX_SIZE);
      a_inserer[0] = '\0';
      strcat(a_inserer, alias->output[i]);
      strcat(a_inserer, location + strlen(alias->input[i]));
      // on le marque comme nécessitant un free ou on vide le tableau d'abord
      if (count_clear_string_al > MAX_STRING_CLEAR - 10) {
        free_alias_orphan_strings();
      }
      string_to_clear_al[count_clear_string_al] = a_inserer;
      count_clear_string_al++;
      // retourner
      return a_inserer;
    }
  }

  // rien trouvé, on retourne la mm chose
  return text;
}

char *str_from_tokens(char **tokens, int nb_token) {
  char *res = malloc(ALIAS_MAX_SIZE);
  res[0] = '\0';

  for (int i = 0; i < nb_token; i++) {
    strcat(res, tokens[i]);
    strcat(res, " ");
  }

  return res;
}

// free some of the sting we returned and could not free ouserlves
void free_alias_orphan_strings() {
  for (int i = 0; i < count_clear_string_al; i++) {
    free(string_to_clear_al[i]);
  }
  count_clear_string_al = 0;
}