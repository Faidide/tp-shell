#ifndef DEF_ALIAS_HPP
#define DEF_ALIAS_HPP

#define ALIAS_MAX 50
#define ALIAS_MAX_SIZE 100
#define MAX_STRING_CLEAR 50

// structure qui memore les alias
struct AliasList {
  char input[ALIAS_MAX][ALIAS_MAX_SIZE];
  char output[ALIAS_MAX][ALIAS_MAX_SIZE];
  int nb;
};

// initialise la structure des alias
struct AliasList *init_aliases();

// ajoute un alias
int add_alias(struct AliasList *alias, char *in, char *out);

// chercher et remplace un alias
char *replace_alias(struct AliasList *alias, char *text);

// retransforme les tokens en une seule chaine
char *str_from_tokens(char **tokens, int nb_token);

// libère des résidues de mémoire
void free_alias_orphan_strings();

#endif // DEF_ALIAS_HPP