#ifndef DEF_VAR_H
#define DEF_VAR_H

#define VAR_MAX 100
#define VAR_MAX_SIZE 100
#define MAX_COMMAND_SIZE 1024
#define MAX_STRING_CLEAR 50

// structure qui memore les variables
struct Variables {
  char key[VAR_MAX][VAR_MAX_SIZE];
  char value[VAR_MAX][VAR_MAX_SIZE];
  int nb;
};

// initialise la structure des alias
struct Variables *init_variables();

// chercher et remplace un alias
char *replace_variables(struct Variables *vars, char *text);

// ajoute une variable
int add_variable(struct Variables *vars, char *key, char *value);

// free some of the sting we returned and could not free ouserlves
void free_orphan_strings();

#endif // DEF_VAR_H