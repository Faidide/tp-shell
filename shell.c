
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "alias.h"
#include "prompt.h"
#include "shell-utils.h"
#include "variables.h"

#define INPUT_BUFFER_SIZE 2048
#define NB_MAX_TOKENS 512

// fonction pour libérer la mémoire en cas de signint
void free_mem(int signo);

struct Variables *variables_ptr;
struct AliasList *aliases_ptr;

int main() {
  /* Une variable pour sotcker les caractères tapés au clavier */
  char line[INPUT_BUFFER_SIZE + 1];

  /* Une variable qui pointera vers les données lues par fgets
   * Sauf problème (erreur, plus de données, ...), elle pointera en
   * fait vers la variable précédente (line) */
  char *data;

  /* Un tableau de chaines où les mots de l'utilisateur seront stockés
   * indépendamment les uns des autres
   * Note: un mot est une suite de lettres séparées par une ou plusieurs
   * espaces des autres mots.  */
  char *tokens[NB_MAX_TOKENS + 1];
  /* variables entières pour comptabiliser le nombre de token */
  int nb_tokens;

  int continue_loop = 1;

  // status pour le wait du child
  int status;

  struct AliasList *aliases = init_aliases();
  struct Variables *variables = init_variables();

  // pointer used by the the signal handler to free those structures
  variables_ptr = variables;
  aliases_ptr = aliases;

  // enregistre la fonction qui va effacer les résidus mémoires pour la sortie

  // c'est complètement inutile, un service va libérer toute la mémoire allouée
  // quand on va fermer le programme, mais parfois, certains profs lancent
  // valgrind et regardent si ils voient des """"""""fuites""""""""" donc je me
  // sens obligé de le mettre
  atexit(free_orphan_strings);
  atexit(free_alias_orphan_strings);
  if (signal(SIGINT, free_mem) == SIG_ERR)
    printf("\ncan't catch SIGINT\n");

  // aliases par défaut
  add_alias(aliases, "gh", "cd ~");
  add_alias(aliases, "ipt", "cat /etc/iptables/iptables.rules");
  add_alias(aliases, "v", "vim");
  add_alias(aliases, "meteo", "curl wttr.in");
  add_alias(aliases, "cheat", "curl cht.sh");
  add_alias(aliases, "cryptos", "curl rate.sx");
  add_alias(aliases, "btc", "curl rate.sx/btc");
  add_alias(aliases, "eth", "curl rate.sx/eth");
  add_alias(aliases, "trx", "curl rate.sx/trx");
  add_alias(aliases, "covid", "curl https://corona-stats.online");
  add_alias(aliases, "map", "telnet mapscii.me");
  add_alias(aliases, "rrl", "nc rya.nc 1987");

  while (continue_loop != 0) {

    /* affiche le prompt */
    display_prompt();

    /* Récupération des données tapées au clavier */
    data = fgets(line, INPUT_BUFFER_SIZE, stdin);

    if (data == NULL) {
      /* Erreur ou fin de fichier : on quitte tout de suite */
      if (errno) {
        /* Une erreur: on affiche le message correspondant
         * et on quitte en indiquant une erreur */
        perror("fgets");
        exit(1);
      } else {
        /* Sinon ça doit être une fin de fichier.*/
        exit(0);
      }
    }

    // remplace les alias
    data = replace_alias(aliases, data);

    // remplace les alias
    data = replace_variables(variables, data);

    /* On vérifie que l'utilisateur n'a pas donné une ligne trop longue */
    if (strlen(data) == INPUT_BUFFER_SIZE - 1) {
      fprintf(stderr, "Input line too long: exiting\n");
      exit(1);
    }

    // autorise les chaines vides
    if (strcmp(data, "\n") == 0) {
      continue;
    }
    // autorise les chaines vides
    if (strcmp(data, "\t") == 0) {
      continue;
    }

    /* On découpe la ligne en tokens (mots)
     * Voir sa documentation dans shell-utils.h (avec les exemples) */
    nb_tokens = split_tokens(tokens, data, NB_MAX_TOKENS);

    /* S'il y a trop de tokens, on abandonne */
    if (nb_tokens == NB_MAX_TOKENS) {
      fprintf(stderr, "Too many tokens: exiting\n");
      exit(1);
    }

    /* S'il n'y a pas de token, c'est que l'utilisateur n'a pas donné de
     * commande. Il n'y a rien à faire. On arrête tout. */
    if (nb_tokens <= 0) {
      fprintf(stderr, "Cannot split tokens: exiting\n");
      exit(1);
    }

    // si l'utilisateur tape exit
    if (strcmp(tokens[0], "exit") == 0) {
      // on met à faux notre variable pour stopper la boucle
      continue_loop = 0;
      // si un appel a cd est fait
    } else if (strcmp(tokens[0], "cd") == 0) {
      // si pas de chemin après cd, ignorer le cd
      if (tokens[1] == NULL)
        continue;
      // appel du changement de répertoire
      if (chdir(tokens[1]) == 0) {
        // all ok, continue
        continue;
      } else {
        perror("chdir");
        continue;
      }
      // si l'utilisateur apelle la commande pour ajouter un alias
    } else if (strcmp(tokens[0], "addalias") == 0) {
      // test si assez d'arguments
      if (nb_tokens < 3) {
        printf("Pas assez d'arguments pour ajouter un alias.\n");
        continue;
      }
      // recupere la chaine qui doit etre remplacé (l'alias)
      char *alias_to_add = tokens[1];
      // recupere la chaine a inserer a la place de l'alias
      char *repl_to_add = str_from_tokens(tokens + 2, nb_tokens - 2);

      // on ajoute l'alias
      int adal = add_alias(aliases, alias_to_add, repl_to_add);
      // test le retour
      if (adal == 1) {
        printf("Nombre max d'alias atteint.\n");
      } else if (adal == 2) {
        // ne devrait pas se produire
        printf("Pas de chaine vide acceptée.\n");
      } else if (adal == 3) {
        printf("Chaine trop longue.\n");
      } else {
        printf("Alias ajouté\n");
      }
      free(repl_to_add);
      continue;
    } else if (strcmp(tokens[0], "setvar") == 0) {
      // test si assez d'arguments
      if (nb_tokens < 3) {
        printf("Pas assez d'arguments pour ajouter une variable.\n");
        continue;
      }
      // recupere la chaine qui doit etre remplacé
      char *var_to_add = tokens[1];
      // recupere la chaine a inserer a la place de la variable
      char *repl_to_add = str_from_tokens(tokens + 2, nb_tokens - 2);

      // on ajoute l'alias
      int adal = add_variable(variables, var_to_add, repl_to_add);
      // test le retour
      if (adal == 1) {
        printf("Nombre max d'alias atteint.\n");
      } else if (adal == 2) {
        // ne devrait pas se produire
        printf("Pas de chaine vide acceptée.\n");
      } else if (adal == 3) {
        printf("Chaine trop longue.\n");
      } else {
        printf("Variable ajout�e�\n");
      }
      free(repl_to_add);
      continue;
    } else {

      // execution d'une commande

      // cherche des redirections
      char *file_out;
      file_out = trouve_redirection(tokens, ">");
      char *file_in;
      file_in = trouve_redirection(tokens, "<");

      int fd_out;
      // si une redirection de sortie a été faite
      if (file_out != NULL) {
        // on va ouvrir ce fichier
        fd_out = open(file_out, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
      }

      int fd_in;
      // si une redirection de sortie a été faite
      if (file_in != NULL) {
        // on va ouvrir ce fichier
        fd_in = open(file_in, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
      }

      // on va fork, et exec ou wait
      pid_t fpid;

      fpid = fork();
      if (fpid > 0) {
        // dans le parent, on doit attendre
        wait(&status);
        // on ferme le fichier de la redirection si elle existe
        if (file_out != NULL) {
          close(fd_out);
        }
        if (file_in != NULL) {
          close(fd_in);
        }
        // analyse du status
        if (WIFSIGNALED(status)) {
          printf("killed by signal=%d%s\n", WTERMSIG(status),
                 WCOREDUMP(status) ? " (dumped core)" : "");
        }
        // test pour savoir si une erreur c'est produite
        if (WEXITSTATUS(status) != 0) {
          // affiche qu'une erreur a été détectée
          printf("Error while running command.\n");
        }
      } else if (!fpid) {
        // si un fichier de redirection a été renseigné, crée la redirection
        if (file_out != NULL) {
          dup2(fd_out, 1);
          dup2(fd_out, 2);
        }
        if (file_in != NULL) {
          dup2(fd_in, 0);
        }
        // dans le fils, on va execvp
        /* On exécute la commande donné par l'utilisateur.
         * Son nom est dans le premier token (le premier mot tapé)
         * ses arguments (éventuels) seront les tokens suivants */
        execvp(tokens[0], tokens);
        // on ne devrait pas arriver ici
        return EXIT_FAILURE;
      } else {
        // erreur lors du fork
        fprintf(stderr, "Internal shell error on fork call.\n");
        perror("fork");
        exit(1);
      }
    }
  }

  free(variables);
  free(aliases);
  exit(0);
}

// signal handler
void free_mem(int signo) {
  free(variables_ptr);
  free(aliases_ptr);
  free_orphan_strings();
  // free_alias_orphan_strings();
  printf("Exit signal received...\n");
  exit(0);
}
