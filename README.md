# TP SHELL
Lisez ce fichier markdown en version formatté sur [GitLab](https://gitlab.com/Faidide/tp-shell)
## Ce que ce shell est capable de faire
### Prompt Personnalisé
Le prompt (ce qui s'affiche avant chaque commande) est personnalisé, il affiche (avec des couleurs) l'username, l'hostname, l'heure, la date, le dossier courant. Il reviens aussi à la ligne pour éviter que des petites fenêtres avec de larges chemins soient pénalisés.

### Aliases
Ce programme permet d'utiliser des alias, qui remplacement une commande par une ou plusieurs autres. On peut mettre des alias comme ceci:
```bash
addalias singe echo ouh ouh ouh ouh !
singe
# va s'afficher: ouh ouh ouh ouh !
```
Les alias par défaut sont:
- **gh**: va au dossier personnel
- **ipt**: afficher les iptables
- **meteo**: affiche la météo locale
- **cryptos**: affiche les prix des cryptomonnaies
- **btc**: affiche le cours du bitcoin (pareil pour trx et eth)
- **covid**: les derniers chiffres du covid
- **map**: une carte interractive ascii de la planète
- **rrl**: de loin l'alias le plus intéressant

Il y a un nombre maximum d'alias possibles.

### Variables:
Il est également possible d'utiliser des variables entourées de *$)VARIABLE($* de la manière suivante:
```bash
setvar progrès retour au singe
echo $)USERNAME($ a décidé quil était passé $)TIME($ et quil était temps daller vers le $)progrès($.
# va s'afficher: quentin a décidé quil était passé 20:28:15 et quil était temps daller vers le retour au singe.
```

Il y a un nombre limité de variables, et certaines sont préconfigurées:
- *TIME*: Donne l'heure
- *DATE*: Donne la date
- *USERNAME*: Nom d'utilisateur
- *HOSTNAME*: Hostname de l'ordinateur

### Redirection vers des fichiers.
Il est possible d'utiliser les chevrons pour rediriger les entrées et sorties vers des fichiers.

### Change directory
La commande *cd* a été implémentée.

### Exit
La commande *exit* a été implémentée.