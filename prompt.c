#include "prompt.h"
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

void display_prompt() {
  // récupère l'heure
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  // récupère le nom d'utilisateur
  char login[40];
  getlogin_r(login, 40);
  // récupère le hostname
  char hostname[40];
  gethostname(hostname, 40);
  // récupère le répertoire courant
  char cwd[PATH_MAX];
  getcwd(cwd, sizeof(cwd));
  printf("\033[01;33m%s\033[0m@\033[01;34m%s\033[0m - "
         "\033[0;31m%d-%02d-%02d\033[0m - \033[01;36m%02d:%02d:%02d\033[0m - "
         "\033[01;32m%s\033[0m\n\033[01;32m->\033[0m ",
         login, hostname, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
         tm.tm_hour, tm.tm_min, tm.tm_sec, cwd);
}