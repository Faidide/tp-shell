#include "variables.h"
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// array to make sure the variables sent back to main get freed
char *string_to_clear[MAX_STRING_CLEAR];
int count_clear_string;

// initialise la structure des vars
struct Variables *init_variables() {

  count_clear_string = 0;

  struct Variables *vars = malloc(sizeof(struct Variables));
  if (vars == NULL) {
    // initialization failed
    perror("malloc");
    exit(2);
  }

  // met les chaines de caractère à zéro
  for (int i = 0; i < VAR_MAX; i++) {
    vars->key[i][0] = '\0';
    vars->value[i][0] = '\0';
    vars->nb = 0;
  }

  return vars;
}

// chercher et remplace un vars (retourn 1 en cas d'erreur)
char *replace_variables(struct Variables *vars, char *text) {

  // on n'a rien besoin de faire si y'a moins de 3 caractères
  if (strlen(text) < 3) {
    return text;
  }

  // pointeur vers ce qu'on va retourner
  char *to_return = text;
  char *to_delete;

  // point to semthing if we do find something
  char *location = NULL;
  char *fin = NULL;
  // test for HOSTNAME variable
  location = strstr(to_return, "$)HOSTNAME($");
  if (location != NULL) {
    // on a trouvé quelquechose
    char *newstring = malloc(strlen(to_return) + strlen("$)HOSTNAME($"));
    // on remplace le premier char a remplacer par un \0
    location[0] = '\0';
    // on recupère le premier charactère de la fin
    fin = location + strlen("$)HOSTNAME($");
    // on ajoute le debut
    newstring[0] = '\0';
    strcat(newstring, to_return);
    // récupère le hostname
    char hostname[40];
    gethostname(hostname, 40);
    strcat(newstring, hostname);
    // ajoute la fin de la chaine
    strcat(newstring, fin);
    to_return = newstring;
  }

  // test for USERNAME variable
  location = strstr(to_return, "$)USERNAME($");
  if (location != NULL) {
    // on a trouvé quelquechose
    char *newstring = malloc(strlen(to_return) + strlen("$)USERNAME($"));
    // on remplace le premier char a remplacer par un \0
    location[0] = '\0';
    // on recupère le premier charactère de la fin
    fin = location + strlen("$)USERNAME($");
    // on ajoute le debut
    newstring[0] = '\0';
    strcat(newstring, to_return);
    // récupère le username
    char login[40];
    getlogin_r(login, 40);
    strcat(newstring, login);
    // ajoute la fin de la chaine
    strcat(newstring, fin);
    to_delete = to_return;
    to_return = newstring;
    if (to_delete != text) {
      free(to_delete);
    }
  }

  // test for DATE variable
  location = strstr(to_return, "$)DATE($");
  if (location != NULL) {
    // on a trouvé quelquechose
    char *newstring = malloc(strlen(to_return) + strlen("$)DATE($"));
    // on remplace le premier char a remplacer par un \0
    location[0] = '\0';
    // on recupère le premier charactère de la fin
    fin = location + strlen("$)DATE($");
    // on ajoute le debut
    newstring[0] = '\0';
    strcat(newstring, to_return);
    // récupère la date
    char date[40];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(date, "%d-%02d-%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
    strcat(newstring, date);
    // ajoute la fin de la chaine
    strcat(newstring, fin);
    to_delete = to_return;
    to_return = newstring;
    if (to_delete != text) {
      free(to_delete);
    }
  }

  // test for TIME variable
  location = strstr(to_return, "$)TIME($");
  if (location != NULL) {
    // on a trouvé quelquechose
    char *newstring = malloc(strlen(to_return) + strlen("$)TIME($"));
    // on remplace le premier char a remplacer par un \0
    location[0] = '\0';
    // on recupère le premier charactère de la fin
    fin = location + strlen("$)TIME($");
    // on ajoute le debut
    newstring[0] = '\0';
    strcat(newstring, to_return);
    // récupère la date
    char date[40];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(date, "%02d:%02d:%02d", tm.tm_hour, tm.tm_min, tm.tm_sec);
    strcat(newstring, date);
    // ajoute la fin de la chaine
    strcat(newstring, fin);
    to_delete = to_return;
    to_return = newstring;
    if (to_delete != text) {
      free(to_delete);
    }
  }

  char buffer_string[VAR_MAX_SIZE + 6];
  for (int i = 0; i < vars->nb; i++) {
    // build a string with the variable wrapped in $) ($
    buffer_string[0] = '\0';
    strcat(buffer_string, "$)");
    strcat(buffer_string, vars->key[i]);
    strcat(buffer_string, "($");
    // test for variable
    location = strstr(to_return, buffer_string);
    if (location != NULL) {
      // on a trouvé quelquechose
      char *newstring = malloc(strlen(to_return) + strlen(buffer_string));
      // on remplace le premier char a remplacer par un \0
      location[0] = '\0';
      // on recupère le premier charactère de la fin
      fin = location + strlen(buffer_string);
      // on ajoute le debut
      newstring[0] = '\0';
      strcat(newstring, to_return);
      // récupère le contenu de la variable
      strcat(newstring, vars->value[i]);
      // ajoute la fin de la chaine
      strcat(newstring, fin);
      to_delete = to_return;
      to_return = newstring;
      if (to_delete != text) {
        free(to_delete);
      }
    }
  }

  // si on renvoie un truc différent du text de dépat
  if (to_return != text) {
    // on le marque comme nécessitant un free ou on vide le tableau d'abord
    if (count_clear_string > MAX_STRING_CLEAR - 10) {
      free_orphan_strings();
    }
    string_to_clear[count_clear_string] = to_return;
    count_clear_string++;
  }

  // fin de la fonction
  return to_return;
}

int add_variable(struct Variables *vars, char *key, char *value) {
  // si on est plein
  if (vars->nb == VAR_MAX) {
    // retourner erreur (1)
    return 1;
  }

  // pas de chaine nulle acceptée
  if (strlen(key) == 0 || strlen(value) == 0) {
    return 2;
  }

  // comtper les longeurs et rejetter les chaines trop longues (>= VAR_MAX_SIZE)
  size_t inlen, outlen;
  inlen = strlen(key);
  outlen = strlen(value);
  if (inlen >= VAR_MAX_SIZE || outlen >= VAR_MAX_SIZE) {
    return 3;
  }

  // variable qui retiens à quel place on insère la variable
  int place = vars->nb;

  // cherche si la variable existe déjà en mémoire
  for (int i = 0; i < vars->nb; i++) {
    if (strcmp(vars->key[i], key) == 0) {
      place = i;
      break;
    }
  }

  // incrémenter nb et strcopier les nouvelles valeurs
  strcpy(vars->key[vars->nb], key);
  strcpy(vars->value[vars->nb], value);

  if (place == vars->nb) {
    vars->nb = vars->nb + 1;
  }

  return 0;
}

// free some of the sting we returned and could not free ouserlves
void free_orphan_strings() {
  for (int i = 0; i < count_clear_string; i++) {
    free(string_to_clear[i]);
  }
  count_clear_string = 0;
}